'''
    General Utilites for the progam
'''
import os
import bcrypt
import platform


# Determines the type of OS
def determineSystem():
    if platform.uname().system == 'Darwin':
        return 'Darwin'
    elif platform.uname().system == 'Window':
        return 'Window'
    elif platform.uname().system == 'Linux':
        return 'Linux'


def encryptPass(username,email,password):
    password = f"{email}/{password}/{username}"
    encryption_suite = bcrypt.hashpw(password.encode(), bcrypt.gensalt(14))
    return encryption_suite.encrypt(password)


def setWorkingDir(USER_HOME, APP_WORKING_DIR_NAME):

    return os.path.join(USER_HOME,APP_WORKING_DIR_NAME)

def setHomeDir(USER_HOME):
    app_home_path = os.path.join(USER_HOME,'.vault/')
    if not os.path.exists(app_home_path):
        os.makedirs(app_home_path)
    return app_home_path