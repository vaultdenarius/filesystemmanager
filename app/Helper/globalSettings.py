from .utilities import determineSystem,setWorkingDir,setHomeDir
from sqlalchemy.ext.declarative import declarative_base
import os

# Defines the type of OS
SYSTEM_TYPE = determineSystem()
# Defines the user home dir
USER_HOME = os.path.expanduser('~')
# App Home Directory
APP_HOME = setHomeDir(USER_HOME)
# Defines the app working dir name
APP_WORKING_DIR_NAME = "Vault_Stash"
# Defines the working dir where all the storage operations take place
WORKING_DIR = setWorkingDir(USER_HOME,APP_WORKING_DIR_NAME)
# Defines the unique user storage folder
# USER_WORKING_DIR
# Defines the directory where db storage takes place
DB_STORAGE_LOCATION = os.path.join(WORKING_DIR,"db_storage")
DB_DEFAULT_DB_NAME = 'vault.fs';
# Defines the directory where executables are stored
EXEC_STORAGE_LOCATION = os.path.join(WORKING_DIR,"bin")
# Defines the directory where user uploaded files are stored
UPLOAD_STORAGE_LOCATION = os.path.join(WORKING_DIR,"uploads")

# Base class for all modules
BASE = declarative_base()