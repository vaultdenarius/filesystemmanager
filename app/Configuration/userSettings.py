from Helper.globalSettings import WORKING_DIR, DB_STORAGE_LOCATION, EXEC_STORAGE_LOCATION, UPLOAD_STORAGE_LOCATION, DB_DEFAULT_DB_NAME
from passlib.hash import pbkdf2_sha512
import json
import os

# TODO handle password hashing
#TODO Make it so that the settings files are placed in their own folder unique to the user
class UserSettings(dict):

    def __init__(self,username='',email='',password=''):
        self.username = username
        self.email = email
        self.password = password
        self.working_dir = WORKING_DIR
        self.db_name = DB_DEFAULT_DB_NAME

    def createRegisteredUserObj(self):
        return {
            'username': self.username,
            'email': self.email,
            'password': self.password,
            'working_dir' : self.working_dir
        }

    def createUserImportantFolders(self):
        # Stores any executables
        os.mkdir(EXEC_STORAGE_LOCATION)
        # Stores the db related files
        os.mkdir(DB_STORAGE_LOCATION)
        # Stores user uploads
        os.mkdir(UPLOAD_STORAGE_LOCATION)

    # TODO need to find a better way to check for this
    def determineIfWorkingDirExists(self):
        return os.path.isdir(self.working_dir)

    # Creates the user Working directory
    def createAppWorkingDir(self):
        if not self.determineIfWorkingDirExists():
            os.mkdir(WORKING_DIR)
            self.createUserImportantFolders()

    # Creates a settings file based on the params passed to the user settings
    def createSettingsFile(self):
        if self.determineIfWorkingDirExists():
            with open(os.path.join(self.working_dir,"settings.json"), 'w') as settingFile:
                json.dump(self.__dict__,settingFile)

    # Reads in a settings file
    def updateUserSettingsFromFile(self):
        self.getDataFromSettingsFile(self)


    # gets Data from Settings file
    def getDataFromSettingsFile(self,obj={}):
        if self.determineIfWorkingDirExists():
            with open(os.path.join(self.working_dir,"settings.json")) as settingFile:
                data = json.load(settingFile)
                for key in data.keys():
                    obj[key] = data[key]
        return obj

    # Print out user settings
    def printUserSettings(self):
        print(self.__dict__)