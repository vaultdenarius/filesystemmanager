from flask import Flask, g
from Db.config_db import ConfigDb
import Db.Modules.relationship
from Server.routes.createUserSetting import creatingUserSettings
from Server.routes.login import loginUser
from Server.routes.logout import logoutUser

app = Flask(__name__)# Creates application instance

# I always need to remember client applications need to explicitly state
# they are sending json over by using their headers

# Closes the Connection after every request
# This is important because only one thread can make updates to the db at a time

'''
    TODO I need to make it so that the aplication has it own configuration file thats 
    not affected by user choices that gets updated with some system level choices the 
    user makes
'''

# @app.teardown_appcontext
# def tear_down_db(err):
# TODO Create method to close db connections
db_config_manager = ConfigDb()


@app.route('/vault/createUser', methods=['POST'])
def createUser():
    return creatingUserSettings(db_config_manager)


@app.route('/vault/login',methods=['POST'])
def login():
    return loginUser(db_config_manager)

@app.route('/vault/logout',methods=['POST'])
def logout():
    return logoutUser(db_config_manager)


# @app.route('/getCoinInfo')


app.run(host="localhost", port=5000, threaded=True)
