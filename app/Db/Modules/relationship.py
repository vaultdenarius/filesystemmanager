from sqlalchemy import Column,Integer, ForeignKey
from sqlalchemy.orm import relationship
from .user import User
from .log import Log
from .transaction import Transaction
from .exchange import Exchange

# Forming a one to many relationship between User and Log
# One coin many log entries
User.logs = relationship('Log')
User.exchanges = relationship('Exchange')
Log.user_id = Column(Integer,ForeignKey('users.id'))

# Forming a bidirectional relationship between the exchanges and their coins
# Forming a bidirectional relationship between exchanges and the user
Exchange.user = relationship('User',back_populates='exchanges')
Exchange.user_id = Column(Integer, ForeignKey('users.id'))
Exchange.coins = relationship('Coin', order_by=Transaction.date, back_populates='exchange')

Transaction.exchange_id = Column(Integer, ForeignKey('exchanges.id'))
Transaction.exchange = relationship('Exchange', back_populates='coins')

