from sqlalchemy import Column, Integer, Text, Boolean
from Helper.globalSettings import BASE
import datetime


class Log(BASE,dict):
    # Table name for logs
    __tablename__ = 'logs'

    id = Column(Integer,primary_key=True)
    active = Column(Boolean, nullable=False)
    logInTime = Column(Text, nullable=False)
    logOffTime = Column(Text)


    # Current date time
    current_date = datetime.datetime.now()

    # This shows the inital state for a user activity log
    def __init__(self):
        self.active = True
        self.logInTime = self.current_date.strftime("%Y-%m-%d %H:%M")
        self.logOffTime = None

    def log_out(self):
        self.active = False
        self.logOffTime = self.current_date.strftime("%Y-%m-%d %H:%M")

    def __repr__(self):
        return "<Activity(active='%s', log_in_time='%s', log_off_time='%s')>" % (
            self.active, self.logInTime, self.logOffTime)
