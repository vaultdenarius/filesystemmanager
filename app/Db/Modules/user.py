from sqlalchemy import Column, Integer, String, Sequence
from passlib.hash import pbkdf2_sha512
from Helper.globalSettings import BASE

class User(BASE,dict):
    # Setting the name of the table in the db
    __tablename__ = 'users'

    # Setting the primary key on the table
    id = Column(Integer,Sequence('user_id_seq'), primary_key=True)

    email = Column(String,nullable=False,unique=True)
    password = Column(String,nullable=False)
    username = Column(String,nullable=False)
    working_dir = Column(String,nullable=False)


    #TODO handle password hashing
    def verify_pass(self,password):
        return pbkdf2_sha512.verify(password,self.password)

    def createDict(self):
        return {
            'id': self.id,
            'email' : self.email,
            'password' : self.password,
            'username' : self.username,
            'working_dir' : self.working_dir
        }

    def __repr__(self):
        return "<User(username='%s', email='%s', password='%s')>" % (self.username, self.email, self.password)