from sqlalchemy import Column, Integer, String, Sequence,Float,Text, Enum
from Helper.globalSettings import BASE

class Transaction(BASE, dict):
    # Name of the table
    __tablename__ = 'transaction'

    id = Column(Integer,Sequence('userCoin_id_seq'),primary_key=True)
    pricePerCoin = Column(Float)
    fee = Column(Float)
    feeCoin = Column(String)
    totalPrice = Column(Float)
    amount = Column(Float)
    date = Column(Text)
    type = Column(Enum('BUY','SELL'))
    method = Column(String)
