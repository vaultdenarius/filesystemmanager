from sqlalchemy import Column, Integer, String, Sequence,Float,Text, ForeignKey, Enum
from Helper.globalSettings import BASE

# TODO add relationship for api links table
class Exchange(BASE,dict):
    # Name of the table
    __tablename__ = 'exchanges'

    id = Column(Integer, Sequence('exchange_id_seq'), primary_key=True)
    exchange = Column(String)
