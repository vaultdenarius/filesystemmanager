from sqlalchemy import Column, Integer, String, Sequence, Text
from Helper.globalSettings import BASE

class Coin(BASE,dict):
    # Name of the table
    __tablename__ = 'coins'

    id = Column(Integer,Sequence('coin_id_seq'),primary_key=True)
    name = Column(String)
    ticker = Column(String)