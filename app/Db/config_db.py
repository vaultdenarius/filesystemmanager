from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Helper.globalSettings import DB_STORAGE_LOCATION,DB_DEFAULT_DB_NAME
from Helper.globalSettings import BASE
import os


class ConfigDb:

    def __init__(self):
        self.db_storage_location = DB_STORAGE_LOCATION
        self.db_name = DB_DEFAULT_DB_NAME
        self.engine = None
        self.session = None

    def update_db_name(self,name):
        self.db_name = name

    def update_db_path(self,path):
        self.db_storage_location = path

    def getDbPath(self):
        return os.path.join(self.db_storage_location,self.db_name)

    def initDb(self):
        self.engine = create_engine(f'sqlite:///{self.getDbPath()}',echo=True)

    def createSchemas(self):
        BASE.metadata.create_all(self.engine)

    def createSession(self):
        self.session = sessionmaker(bind=self.engine)
        return self.session