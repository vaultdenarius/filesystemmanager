from flask import request, jsonify
from sqlalchemy import and_
from Db.Modules.user import User
from Db.Modules.exchange import Exchange
from Configuration.userSettings import UserSettings
from Server.modules.hermes import Hermes


# TODO add checks for
def userExists(session,user_auth):
    # TODO I need to check the validity of this method
    # Queries the user list to see if a user exists by the email and password
    return session.query(User).filter(and_(User.email == user_auth['email'], User.password == user_auth['password'])).first()


def loginUser(db_config_manager):
    '''
        Authenticates the user into the app

        {
            email,
            password
        }
    '''
    # Gets the request with the user auth info
    user_auth = request.get_json()

    userSettings = UserSettings()
    if userSettings.determineIfWorkingDirExists() is False:
        return jsonify(
            {
                'fail': False
            }
        )

    # Start up DB
    db_config_manager.initDb()

    Session = db_config_manager.createSession();

    session = Session()

    user = userExists(session,user_auth)
    # TODO work on better way to return errors
    if user is None:
        return jsonify(
            {
                'fail': False
            }
        )


    # TODO work on setting up application settings

    # baseUserSettings = UserSettings(**user.createDict())

    # loadedUserFileSettings = baseUserSettings.getDataFromSettingsFile()

    # print(loadedUserFileSettings)

    exchanges = session.query(Exchange).filter(Exchange.user_id == user.id).all()

    print(user.createDict())

    return jsonify(
        Hermes(user.createDict(),exchanges).createJsonObject()
    )



