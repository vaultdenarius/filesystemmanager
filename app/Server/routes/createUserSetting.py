from flask import request, jsonify
from Db.Modules.user import User
from Db.Modules.log import Log
from Configuration.userSettings import UserSettings
from Server.modules.hermes import Hermes

def creatingUserSettings(db_config_manager):
    '''
        Creates user settings and stores them in an app folder
        Registers the user and returns the user active state
        :return:
    '''
    # Once the user tries to create a new account
    # We'll first take their credentials and create a user settings object

    '''
        Lets start... First we're getting a json object from the user/
        This json object should contain

        {
            username:
            email:
            password:
        }

        Then I'm going to create a user_settings object derived from that json object.
        Configuration data will be created unique to the users information and then
        a settings folder with other important information will be created
    '''
    user_credentials = request.get_json()

    # Annoying issue can be resolved later
    # user_credentials['password'] = encryptPass(**user_credentials)

    # Now we're going to create the users settings object
    user_settings = UserSettings(**user_credentials)

    # Checking to see if the working directory exists
    if not user_settings.determineIfWorkingDirExists():
        '''
                Some extra work needs to be added in here

                TODO - in the event there are multiple users in the system
                       make seperate dirs under the working dir for each user
        '''
        user_settings.createAppWorkingDir()

        # Create the users settings file
        user_settings.createSettingsFile()

    # # Initialize connection to db
    db_config_manager.initDb()

    # Creates All the table schemas needed for the system
    # By this time all the other db modules would have been loaded
    db_config_manager.createSchemas()

    # Create Session object to connect to the db
    Session = db_config_manager.createSession()

    session = Session()

    # Creates and adds the user to the db
    registeredUser = User(**user_settings.createRegisteredUserObj())
    registeredUser.logs.append(Log())
    session.add(registeredUser)

    # Persists user to the db
    session.commit()


    return jsonify(
        Hermes(registeredUser.createDict()).createJsonObject()
    )
