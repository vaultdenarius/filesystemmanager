from flask import request, jsonify
from sqlalchemy import and_
from Db.Modules.user import User
from Db.Modules.log import Log

def logoutUser(db_config_manager):
    '''
        Handles the logging out of the user from the application
    :param db_config_manager:
    :return:
    '''

    # Gets the user info passed to it from client
    user_info = request.get_json()

    # Init the connection to the db
    db_config_manager.initDb()

    # Create Session object to connect to the db
    Session = db_config_manager.createSession()

    session = Session()

    # Searches for the most recent activity log
    activity_log = session.query(Log).filter(and_(Log.user_id == user_info['user']['id']), Log.logOffTime is None).all()

    print(activity_log)
    # # logs user out
    # activity_log.log_out()
    #
    # # Save changes to db
    # session.commit()

    return jsonify({
        'log': 'off'
    })