
# Response object that gets sent back to the client
# When they log in
class Hermes(dict):

    # User Object
    # List of Exchanges
    def __init__(self,user,exchanges=None):
        self.user = user
        if exchanges is None:
            exchanges = []
        self.exchanges = exchanges

    def createJsonObject(self):
        print(self.user)
        return {
            'user': self.user,
            'exchanges' : self.exchanges
        }