#Setting up Backend scripts

1. We'll start by having a script that handles authenticating the user files
    1. In the event the user has multiple users using the same system the software should provide the ability
        to be able to securely access different user files without any user being able to affect the functions
        of another user files
    2. Auth Scripts
        1. Create_Account
            1. This script will, using the users hashed password, be able to create target folders unique to this users account
            2. Grants users access to the target folders

        2. Login
            1. This script, using the users pass will grant the application access to the users target folders
        3. Delete_Account
            1. This will delete all instances of the user target folder
    3. Helper Scripts
        1. Create locked files
            1. Takes in user password to be able to encrypt files and also create target folder
        2. Access Locked files
            1. Using the Users Pass, login into the users target folder giving the user access to their target folders
        3. Create Db
            1. Creates db unique to the user
        4. Communicate with Front end
            1. This script will allow the smooth communication between the backend and the front end of the application
    4. Api Script
        1. Script to give access to db entries

