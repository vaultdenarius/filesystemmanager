from distutils.core import setup

setup(
    # Application name:
    name='fileSystemManager',

    # Version number (Inital)
    version='0.1.0',

    # Application Author details
    author='Xavier Thomas',
    author_email='xthom001@gmail.com',

    # Packages
    packages=["app"],


    # Details
    url="",

    license="LICENSE.txt",

    ong_description=open("README.txt").read(),


)